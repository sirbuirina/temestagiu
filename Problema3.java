package Problema3;

public class Problema3 {

	//intrare-->vectorul de intrare
	//data-->vector temporar de stocare
	//start,end-->index de inceput/sfarsit al intrarii
	//index-->indexul curent in vectorul data
	//r-->dimensiunea combinarii
	static int cnt=0;
	static int sume[]=new int[50];//vectorul in care se stozheaza toate sumele combinarilor
    static void combinari(int intrare[], int data[], int start,
                                int end, int index, int r)
    {
    	int sum=0;
        if (index == r)
        {
        	System.out.print("Combinarea " + cnt + ": " );
            for (int j=0; j<r; j++)
            {
                System.out.print(data[j]+" ");
                sum=sum+data[j];
                sume[cnt]=sum;
            }
            cnt++;       
            System.out.println("-----> Suma elementelor este: " + sum);
            return;
        }
 
       
        for (int i=start; i<=end && end-i+1 >= r-index; i++)
        {
            data[index] = intrare[i];
            combinari(intrare, data, i+1, end, index+1, r);
        }
    }
 
   
    static void print(int arr[], int n, int r)
    {
        int data[]=new int[r];
        combinari(arr, data, 0, n-1, 0, r);
    }
 
	public static void main(String[] args) {
		    int array[] = {1,2,3,4};
	        int r = 3;
	        int n = array.length;
	        print(array, n, r);
	        System.out.println("Numarul total de combinari: " + cnt );
	        System.out.print("Vectorul sumelor: ");
	        for(int i=0;i<cnt;i++)
	        	System.out.print(sume[i]+ " ");
	        int max=sume[0];
	        for(int i=1;i<cnt;i++)
	        {
	        	if(sume[i]>max)
	        		max=sume[i];
	        }
	       
	        System.out.print("\nSuma maxima este: " + max);
	}

}
