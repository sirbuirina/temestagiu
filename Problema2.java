package Problema2;

import java.util.Scanner;

public class Problema2 {

	@SuppressWarnings("resource")
	public static void main(String[] args) { 
		int n;
		int matrice[][]=new int[50][50];
		int i,j;
		System.out.print("Numarul de randuri de afisat = ");
		Scanner scan = new Scanner(System.in);
		n = scan.nextInt();	
		for(j=0;j<n;j++)
		{
			for(i=0;i<=j;i++)
			{
				if(j==i || i==0)
				{
					matrice[j][i]=1;
				}
				else
				{
					matrice[j][i]=matrice[j-1][i-1]+matrice[j-1][i];
				}
				System.out.print(matrice[j][i]+" ");
			}
			System.out.print("\n");
		}
	   
	}

}
