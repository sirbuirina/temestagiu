package Problema5;

import java.util.Scanner;

public class Problema5 {
	
    //ti-->tija initiala
	//tf-->tija finala
	//ta-->tija auxiliara
	static void hanoi(int n,char ti,char tf,char ta)
	{
		if(n==1)
		{
			System.out.println("Mutam de pe "+ti+" pe "+tf);
		}
		else
		{
			hanoi(n-1,ti,ta,tf);
			System.out.println("Mutam de pe "+ti+" pe "+tf);
			hanoi(n-1,ta,tf,ti);
		}
	}
	
	static void hanoi2(int n)
	{
		for(int i=1;i<(1<<n);i++)
		{
			System.out.println("Mutam de pe "+ (i&i-1)%3 +" pe "+((i|i-1)+1)%3);
		}
	}
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Numar de discuri= ");
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		hanoi(n,'a','b','c');
	}

}
