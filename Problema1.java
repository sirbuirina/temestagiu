package Problema1;

import java.util.Scanner;

public class Problema1 {

	static void rotate(int data[],int n,int m)
	{
		int temp,i,j;
		for(i=0;i<m;i++)
		{
			temp=data[0];
			for(j=1;j<=(n-1);j++){
				data[j-1]=data[j];
			}
			data[n-1]=temp;
		}
	}
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		int a[][]=new int[50][50];
		int data[]=new int[50];
		int i,j,n,m,k;
		System.out.print("Rangul matricii n = ");
		Scanner scan = new Scanner(System.in);
		n = scan.nextInt();
		System.out.print("Numarul de afisat m = ");
		m = scan.nextInt();
		System.out.print("Introdu cele n numere : ");
		
	
		for(i=0;i<n;i++)
		{
			data[i] = scan.nextInt();
		}
		for(i=0;i<n;i++)
		{
			for(j=0;j<n;j++)
			{
				a[i][j]=data[j];
			}
		}
	
		
		for(i=0;i<n;i++)
		{
			rotate(a[i],n,i);
			
		}
	
		for(k=0;k<m;k++)
		{
			for(i=0;i<n;i++)
			{
				for(int x=0;x<n;x++)
				{
					data[x]=a[n-1][x];
				}
				for(j=0;j<n;j++)
				{
					System.out.print(a[i][j]+" ");
					a[i][j]=data[j];
				}
				System.out.print("\n");
				rotate(a[i],n,i);
				
			}
			System.out.print("\n");
		}
		
	}

}
